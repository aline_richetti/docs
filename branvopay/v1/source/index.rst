.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:
   :glob:

   bankslip/introduction
   bankslip/integration
   bankslip/notification
   bankslip/status-table

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:
   :glob:

   card/introduction
   card/integration
   card/notification
   card/status-table

######
Início
######

Bem vindo à documentação das API's de pagamento da Branvo.

Dirija-se à seção desejada no menu lateral.