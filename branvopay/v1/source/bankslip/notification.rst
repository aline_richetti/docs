.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:
   
###########
Notificação
###########

A URL de notificação do boleto é o endereço através do qual a API da BranvoPay notificará a aplicação do usuário sobre
atualizações nos seus boletos.

A aplicação do usuário deverá manter a URL de notificação preparada para receber via GET os
seguintes parâmetros:

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | numero                     | string          | | Número do boleto                                         |
   |                            |                 | | Este campo é o “Nosso Número” gerado pelo banco, e não o |
   |                            |                 | | número do pedido                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | valorRecebido              | decimal         | Valor pago pelo pagador                                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | status                     | int             | | Status do boleto                                         |
   |                            |                 | | Consultar a tabela de status de boleto                   |
   +----------------------------+-----------------+------------------------------------------------------------+
   | vencimento                 | string          | Data de vencimento. Formato: dd/mm/YYYY                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | statusDescription          | string          | Descrição do Status                                        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | dataPagamento              | string          | | Data de pagamento do boleto                              |
   |                            |                 | | Formato: dd/mm/YYYY                                      |
   |                            |                 | | Em caso de cancelamento, este campo conterá a data do    |
   |                            |                 | | cancelamento                                             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | multa                      | decimal         | Valor de multa + juros pago pelo cliente, se houver        |
   +----------------------------+-----------------+------------------------------------------------------------+

Este envio será disparado automaticamente pela API assim que houver atualizações em um boleto (liquidação, baixa).

Para realizar os testes, recomendamos o uso da ferramenta POSTMAN ou do próprio browser, enviando os parâmetros acima 
via GET para a sua URL de notificação.