.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

##########
Introdução
##########
 
******
Resumo
******

Este documento tem o objetivo de auxiliar na integração com a API da plataforma
BranvoPay – Cartão de Crédito. Aqui, o desenvolvedor encontrará todas as
especificações técnicas do que precisa para realizar transações de cartão de
crédito, cancelar/estornar transações e fazer transações com split de pagamentos
e pagamentos recorrentes (em breve) através da plataforma.

***********************
Descrição da plataforma
***********************

A API BranvoPay – Cartão de Crédito é utilizada para realizar vendas e cobranças
online através de cartão de crédito, com as modalidades de split de pagamentos
e pagamento recorrente (em breve).

Atualmente, a API encontra-se na versão 1 (v1), para todos os serviços disponíveis.

=========
Glossário
=========

O presente glossário visa explicar alguns termos utilizados nesta documentação,
para facilitar o entendimento por parte de todos que realizarem a leitura e evitar
dúvidas no momento da integração.

*Cliente/Comprador* – É o cliente final da loja, aquele que comprou e realizou o
pagamento com seu cartão de crédito/boleto.

*Loja/Usuário BranvoPay* – É o usuário cadastrado na plataforma BranvoPay, aquele
que recebe os pagamentos. (Estabelecimentos)

*Transação autorizada/aprovada* – A transação foi enviada à
operadora/adquirente e foi apenas **AUTORIZADA**, ou seja, recebemos o OK para
continuar com a operação, mas ela ainda não foi debitada/creditada e enviada
para a fatura do cliente. O real crédito/débito da transação acontece no momento
da **CAPTURA**, podendo ela ser automática ou não, conforme descrito
posteriormente. Este status NÃO garante a confirmação do pagamento.

*Transação capturada* – A transação foi enviada à operadora/adquirente e foi
**CAPTURADA**, ou seja, neste momento é que o valor foi creditado/debitado do
cliente. Atente para iniciar a logística posterior ao pagamento apenas quando a
transação estiver com este status. Este status GARANTE a confirmação do
pagamento, sendo que status anteriores a este NÃO garantem.

=========
Endpoints
=========

Para realização da integração com a API estão disponíveis 2 endpoints, um para testes, e outro para
o ambiente de produção.


**Testes**

``https://sandbox-gateway.branvo.com.br/api/``

**Produção**

``https://gateway.branvo.com/api/``

Recomendamos iniciar a integração no ambiente sandbox, e apenas migrar para o ambiente de produção
depois que a integração estiver completamente testada e validada. Não é necessária homologação e
testes por parte da BRANVO, porém, é necessário solicitar o cadastro do usuário na BranvoPay para
obtenção do Token de acesso, para utilizá-lo em produção.

===========
Requisições
===========

As requisições para a API são realizadas sempre via GET, sendo que os retornos são todos via JSON.
O JSON de retorno sempre possuirá a seguinte estrutura:

.. code:: javascript

    {
        "success": true,
        "data": {
            ...
        }
    }

Sendo que “success” é um campo do tipo boolean, que retornará true caso a operação seja executada
com sucesso, e false em caso de algum problema.

O campo “data” pode conter 2 formas de dados.
Em caso de falha, conterá apenas um campo “message”, com a mensagem de erro. Em caso de sucesso,
conterá as informações solicitadas, conforme cada operação da API.

A API retornará um código HTTP 200 - OK em caso de sucesso, 400 – Bad Request em caso de alguma
falha e 403 - Forbidden em caso de problemas de autenticação.