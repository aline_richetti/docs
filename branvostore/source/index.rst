.. Branvo Store documentation master file
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Menu:

   transaction/index

##########
Introdução
##########

A API da Branvo Store tem o objetivo de comunicar-se com demais sistemas internos da Branvo, fornecendo dados como transações realizadas, pacotes, recursos, entre outros.

##########
Changelog
##########

.. table::
   :widths: auto

   +--------------------------+-----------------+---------------------------------------------------+
   | Data                     | Versão          | Descrição                                         |
   +==========================+=================+===================================================+
   | | 02/01/2019             | | 1.0.0         | | Primeira versão da documentação com os detalhes |
   | |                        | |               | | da operação de consulta de transações           |
   +--------------------------+-----------------+---------------------------------------------------+